if not WarBoard_Inf then WarBoard_Inf = {} end
local WarBoard_Inf = WarBoard_Inf
local LabelSetText, WindowSetShowing, WindowSetShowing, WindowSetAlpha, WindowGetShowing, GetZoneName, GetZoneAreaName, 
	  GetInfluenceData, Tooltips, GetStringFormat, GetAreaData, towstring, tostring, format, GetString, ipairs = 
	  LabelSetText, WindowSetShowing, WindowSetShowing, WindowSetAlpha, WindowGetShowing, GetZoneName, GetZoneAreaName, 
	  DataUtils.GetInfluenceData, Tooltips, GetStringFormat, GetAreaData, towstring, tostring, string.format, GetString, ipairs

local UpdateInfluenceBarHook

function WarBoard_Inf.Initialize()
	if WarBoard.AddMod("WarBoard_Inf") then
		StatusBarSetForegroundTint("WarBoard_InfPercentBar", 0, 185, 174)
		LabelSetTextColor("WarBoard_InfTitle2", 0, 185, 174)

		UpdateInfluenceBarHook = EA_Window_PublicQuestTracker.UpdateInfluenceBar
		EA_Window_PublicQuestTracker.UpdateInfluenceBar = WarBoard_Inf.UpdateInfluenceBar

		RegisterEventHandler(SystemData.Events.PLAYER_INFLUENCE_UPDATED, "WarBoard_Inf.OnPlayerInfUpdate")
		RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "WarBoard_Inf.OnPlayerInfUpdate")
		RegisterEventHandler(SystemData.Events.PLAYER_AREA_CHANGED, "WarBoard_Inf.OnPlayerInfUpdate")
		WarBoard_Inf.OnPlayerInfUpdate()
	end
end

function WarBoard_Inf.UpdateInfluenceBar()
	local showBar = DataUtils.UpdateInfluenceBar("WarBoard_InfPercentBar", EA_Window_PublicQuestTracker.GetLocalAreaInfluenceID())
	local infID, hasPQButNoInfluence = EA_Window_PublicQuestTracker.GetLocalAreaInfluenceID()

	-- Show or hide the entire window depending on whether we're in an influence area or a PQ area without influence.
	WindowSetShowing("EA_Window_PublicQuestTracker", showBar or hasPQButNoInfluence)
	EA_Window_PublicQuestTracker.UpdateMainWindowSize()
end

function WarBoard_Inf.OnPlayerInfUpdate()
	local infId = WarBoard_Inf.GetLocalAreaInfID()
	if infId then
		DataUtils.UpdateInfluenceBar("WarBoard_InfPercentBar", infId)
		WarBoard_Inf.ShowBar()

		local raceLoc = StringUtils.GetFriendlyRaceForCurrentPairing(zonePairing, true)
		local chapNum = GetChapterShortName(infId)
		LabelSetText("WarBoard_InfName", towstring(format("%s %s", tostring(raceLoc), tostring(chapNum))))
		LabelSetText("WarBoard_InfTitle", L"")
		LabelSetText("WarBoard_InfTitle2", L"")
	else
		--non-influence area
		LabelSetText("WarBoard_InfName", L"")
		LabelSetText("WarBoard_InfTitle", L"WarBoard")
		LabelSetText("WarBoard_InfTitle2", L"Influence")
		WarBoard_Inf.HideBar()
	end
	WindowSetShowing("EA_Window_PublicQuestTrackerInfluenceBar",false)
    WindowSetShowing("EA_Window_PublicQuestTrackerLocation",false)
end

function WarBoard_Inf.ShowBar()
	WindowSetShowing("WarBoard_InfPercentBar", true)
	WindowSetAlpha("WarBoard_InfBarBackground", 0.25)
	WindowSetShowing("WarBoard_InfPercentBarReward1", true)
	WindowSetShowing("WarBoard_InfPercentBarReward2", true)
	WindowSetShowing("WarBoard_InfPercentBarReward3", true)
end

function WarBoard_Inf.HideBar()
	WindowSetShowing("WarBoard_InfPercentBar", false)
	WindowSetAlpha("WarBoard_InfBarBackground", 0)
	WindowSetShowing("WarBoard_InfPercentBarReward1", false)
	WindowSetShowing("WarBoard_InfPercentBarReward2", false)
	WindowSetShowing("WarBoard_InfPercentBarReward3", false)
end

function WarBoard_Inf.OnMouseButton()
	local influenceId = WarBoard_Inf.GetLocalAreaInfID()
	local influenceData = GetInfluenceData(influenceId)
	if WindowGetShowing("TomeWindow") then
		WindowSetShowing("TomeWindow", false)
	else
		if influenceData.tomeSection ~= nil then
			TomeWindow.OpenTomeToEntry(influenceData.tomeSection, influenceData.tomeEntry)
		else
			TomeWindow.OpenTomeToEntry(0,0)
		end
	end
end

function WarBoard_Inf.OnMouseOver()
	local influenceId = WarBoard_Inf.GetLocalAreaInfID()

	local influenceData = GetInfluenceData(influenceId)
	if influenceData == nil then
		return
	end


	local zoneName = GetZoneName(influenceData.zoneNum)
	local areaName = GetZoneAreaName(influenceData.zoneNum, influenceData.zoneAreaNum)

	local text = GetStringFormat(StringTables.Default.TEXT_INFLUENCE_DESC,
								{ areaName, zoneName,
								  influenceData.rewardLevel[1].amountNeeded,
								  influenceData.npcName,
								  influenceData.rewardLevel[2].amountNeeded,
								  influenceData.rewardLevel[3].amountNeeded })
	Tooltips.CreateTextOnlyTooltip(SystemData.ActiveWindow.name, nil) 
	Tooltips.AnchorTooltip(WarBoard.GetModToolTipAnchor("WarBoard_Inf"))

	-- Name
	local row = 1
	local column = 1
	Tooltips.SetTooltipText(row, column, GetString(StringTables.Default.LABEL_AREA_INFLUENCE))
	row = row + 1

	-- Current Points
	local curPointsText = GetStringFormat(StringTables.Default.TEXT_CURRENT_INFLUENCE, { influenceData.curValue })
	Tooltips.SetTooltipText(row, column, curPointsText)
	Tooltips.SetTooltipColor(row, column, 255, 204, 102)
	row = row + 1

	-- Rewards Avail / Recieved
	if  influenceData.rewardLevel[1].rewardsRecieved then
		Tooltips.SetTooltipText(row, column, GetString(StringTables.Default.TEXT_BASIC_REWARD_RECEIVED))
		Tooltips.SetTooltipColor(row, column, 150, 150, 150)
		row = row + 1
	elseif influenceData.curValue >= influenceData.rewardLevel[1].amountNeeded then
		Tooltips.SetTooltipText(row, column, GetString(StringTables.Default.TEXT_BASIC_REWARD_AVAILIABLE))
		Tooltips.SetTooltipColor(row, column, 255, 255, 0)
		row = row + 1
	else
		Tooltips.SetTooltipText(row, column, L"Needed for Basic Reward: ")
		Tooltips.SetTooltipText(row, 3, towstring(influenceData.rewardLevel[1].amountNeeded - influenceData.curValue)) 
		Tooltips.SetTooltipColor(row, column, 150, 150, 150)
		row = row + 1
	end

	if influenceData.rewardLevel[2].rewardsRecieved then
		Tooltips.SetTooltipText(row, column, GetString(StringTables.Default.TEXT_ADVANCED_REWARD_RECEIVED))
		Tooltips.SetTooltipColor(row, column, 150, 150, 150)
		row = row + 1
	elseif influenceData.curValue >= influenceData.rewardLevel[2].amountNeeded then
		Tooltips.SetTooltipText(row, column, GetString(StringTables.Default.TEXT_ADVANCED_REWARD_AVAILIABLE))
		Tooltips.SetTooltipColor(row, column, 255, 255, 0)
		row = row + 1
	else
		Tooltips.SetTooltipText(row, column, L"Needed for Advanced Reward: ")
		Tooltips.SetTooltipText(row, 3, towstring(influenceData.rewardLevel[2].amountNeeded - influenceData.curValue)) 
		Tooltips.SetTooltipColor(row, column, 150, 150, 150)
		row = row + 1
	end

	if influenceData.rewardLevel[3].rewardsRecieved then
		Tooltips.SetTooltipText(row, column, GetString(StringTables.Default.TEXT_ELITE_REWARD_RECEIVED))
		Tooltips.SetTooltipColor(row, column, 150, 150, 150)
		row = row + 1
	elseif influenceData.curValue >= influenceData.rewardLevel[3].amountNeeded then
		Tooltips.SetTooltipText(row, column, GetString(StringTables.Default.TEXT_ELITE_REWARD_AVAILIABLE))
		Tooltips.SetTooltipColor(row, column, 255, 255, 0)
		row = row + 1
	else
		Tooltips.SetTooltipText(row, column, L"Needed for Elite Reward: ")
		Tooltips.SetTooltipText(row, 3, towstring(influenceData.rewardLevel[3].amountNeeded - influenceData.curValue)) 
		Tooltips.SetTooltipColor(row, column, 150, 150, 150)
		row = row + 1
	end

	Tooltips.Finalize()
end

function WarBoard_Inf.GetLocalAreaInfID()
	local areaData = GetAreaData()

	if not areaData then
		-- DEBUG(L"[EA_Window_PublicQuestTracker.GetLocalAreaInfluenceID] AreaData returned nil")
		return nil
	end

	for key, value in ipairs(areaData) do
		-- These should match the data that was retrived from war_interface::LuaGetAreaData
		if value.influenceID ~= 0 then
			return value.influenceID
		end
	end

	return nil
end
