<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="WarBoard_GP" version="0.4" date="05/11/2009" >
		<Author name="Snotrocket" email="snotrocketwar@gmail.com" />
		<Description text="Guild Point mod for WarBoard." />
		<VersionSettings gameVersion="1.3.5" windowsVersion="1.0" />
		<Dependencies>
			<Dependency name="WarBoard" />
		</Dependencies>
		<Files>
			<File name="WarBoard_GP.lua" />
			<File name="WarBoard_GP.xml" />
		</Files>
		<OnInitialize>
			<CallFunction name="WarBoard_GP.Initialize" />
		</OnInitialize>

		<WARInfo>
	<Categories>
		<Category name="RVR" />
		<Category name="OTHER" />
	</Categories>
	<Careers>
		<Career name="BLACKGUARD" />
		<Career name="WITCH_ELF" />
		<Career name="DISCIPLE" />
		<Career name="SORCERER" />
		<Career name="IRON_BREAKER" />
		<Career name="SLAYER" />
		<Career name="RUNE_PRIEST" />
		<Career name="ENGINEER" />
		<Career name="BLACK_ORC" />
		<Career name="CHOPPA" />
		<Career name="SHAMAN" />
		<Career name="SQUIG_HERDER" />
		<Career name="WITCH_HUNTER" />
		<Career name="KNIGHT" />
		<Career name="BRIGHT_WIZARD" />
		<Career name="WARRIOR_PRIEST" />
		<Career name="CHOSEN" />
		<Career name= "MARAUDER" />
		<Career name="ZEALOT" />
		<Career name="MAGUS" />
		<Career name="SWORDMASTER" />
		<Career name="SHADOW_WARRIOR" />
		<Career name="WHITE_LION" />
		<Career name="ARCHMAGE" />
	</Careers>
</WARInfo>
	</UiMod>
</ModuleFile>