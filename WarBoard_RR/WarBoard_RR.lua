if not WarBoard_RR then WarBoard_RR = {} end
local WarBoard_RR = WarBoard_RR
local WindowSetAlpha, LabelSetTextColor, LabelSetText, StatusBarSetCurrentValue, StatusBarSetMaximumValue, Tooltips, format, tostring,
	  towstring =
	  WindowSetAlpha, LabelSetTextColor, LabelSetText, StatusBarSetCurrentValue, StatusBarSetMaximumValue, Tooltips, string.format, tostring,
	  towstring

function WarBoard_RR.Initialize()
	if WarBoard.AddMod("WarBoard_RR") then
		StatusBarSetForegroundTint("WarBoard_RRPercentBar", 204, 126, 201)
		WindowSetAlpha("WarBoard_RRBarBackground", 0.25)
		LabelSetTextColor("WarBoard_RRStat", 200, 50, 200)

		RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_UPDATED, "WarBoard_RR.OnPlayerRpUpdate")
		--PLAYER_RENOWN_TITLE_UPDATED
		WarBoard_RR.OnPlayerRpUpdate()
	end
end

function WarBoard_RR.OnPlayerRpUpdate()
	if GameData.Player.Renown.curRank < 255 then
		local RpEarned = GameData.Player.Renown.curRenownEarned
		local RpNeeded = GameData.Player.Renown.curRenownNeeded
		local RpPercent = RpEarned / RpNeeded * 100
		local curRR = GameData.Player.Renown.curRank
		LabelSetText("WarBoard_RRName", towstring(format("RR: %s", tostring(curRR))))
		LabelSetText("WarBoard_RRStat", towstring(format("%.2f%%", RpPercent)))
		LabelSetText("WarBoard_RRTitle", L"")
		LabelSetText("WarBoard_RRTitle2", L"")

		StatusBarSetCurrentValue("WarBoard_RRPercentBar", RpEarned)
		StatusBarSetMaximumValue("WarBoard_RRPercentBar", RpNeeded)
	else
		-- We've reached the maximum level.
		LabelSetText("WarBoard_RRTitle", L"Renown Rank")
		LabelSetText("WarBoard_RRTitle2", L"255")
		LabelSetText("WarBoard_RRName", L"")
		LabelSetText("WarBoard_RRStat", L"")
		LabelSetTextColor("WarBoard_RRTitle", 200, 500, 200)
		WindowSetAlpha("WarBoard_RRBarBackground", 0)
	end
end

function WarBoard_RR.OnMouseOver()
	local RpEarned = GameData.Player.Renown.curRenownEarned
	local RpNeeded = GameData.Player.Renown.curRenownNeeded
	local curRR = GameData.Player.Renown.curRank
	local curTitle = GameData.Player.Renown.curTitle 
	Tooltips.CreateTextOnlyTooltip("WarBoard_RR", nil)
	Tooltips.AnchorTooltip(WarBoard.GetModToolTipAnchor("WarBoard_RR"))
	Tooltips.SetTooltipColor(1, 1, 255, 255, 255)
	Tooltips.SetTooltipColor(4, 3, 0, 200, 0)
	Tooltips.SetTooltipColor(5, 3, 247, 194, 088)
	Tooltips.SetTooltipText(1, 1, towstring(format("Renown Rank: %s", tostring(curRR))))
	Tooltips.SetTooltipText(2, 1, towstring(format("Renown Title: %s", tostring(curTitle))))
	Tooltips.SetTooltipText(3, 1, towstring(format("Total required for rank %s: ", tostring(curRR + 1))))
	Tooltips.SetTooltipText(3, 3, towstring(RpNeeded))

	Tooltips.SetTooltipText(4, 1, towstring(format("Earned toward rank %s: ", tostring(curRR + 1))))
	Tooltips.SetTooltipText(4, 3, towstring(RpEarned))

	Tooltips.SetTooltipText(5, 1, towstring(format("Needed for rank %s: ", tostring(curRR + 1))))
	Tooltips.SetTooltipText(5, 3, towstring(RpNeeded-RpEarned))
	Tooltips.Finalize()
end
